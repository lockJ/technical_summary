package cn.wechat.utils;

import cn.ihkappweb.versionsthree.vo.AppV3BaseVo;
import cn.wechat.vo.WeChatBaseVo;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by RXJ on 2018/4/12.
 */
public class ClazzMapUtils {

    public static HashMap<String,String>  ClazzChangeMap(Object bean)throws Exception{
        HashMap<String,String> map = new HashMap<String,String>();
        Class type = bean.getClass();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);
        PropertyDescriptor[] propertyDescriptors =beanInfo.getPropertyDescriptors();
        for(PropertyDescriptor descriptor :propertyDescriptors){
            String propertyName = descriptor.getName();
            if(!propertyName.equals("class")){
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean);
                if(result != null){
                    map.put(propertyName ,result.toString());
                }
            }
        }
        return map;
    }

    public static Object MapChangeClazz(Class type,Map map )throws Exception{
        BeanInfo beanInfo = Introspector.getBeanInfo(type);
        Object obj = type.newInstance();
        PropertyDescriptor[] propertyDescriptors =beanInfo.getPropertyDescriptors();
        for(PropertyDescriptor descriptor :propertyDescriptors){
            String propertyName = descriptor.getName();
            if(map.containsKey(propertyName)){
                Object value = map.get(propertyName);
                descriptor.getWriteMethod().invoke(obj,value);
            }
        }

        return obj;
    }

    /**
         * 将对象装好为Map
         *
         * @param obj
         * @return
         * @throws Exception
         */
        public static HashMap<String, String> objectToMap(Object obj) throws Exception {
            if (obj == null) {
                return null;
            }

            HashMap<String, String> map = new HashMap<String, String>();

            Class clazz = obj.getClass();
            for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
                Field[] declaredFields = clazz.getDeclaredFields();
                for (Field field : declaredFields) {
                    field.setAccessible(true);
                    if (StringTools.isNotempty(field.get(obj))) {
                        map.put(field.getName(), field.get(obj).toString());
                    }

                }
            }

            return map;
        }

        /**
         * 将Map装好为obj
         *
         * @param clazz
         * @param map
         * @return
         * @throws Exception
         */
        public static Object mapToObject(Class clazz, Map map) throws Exception {
            if (map == null) {
                return null;
            }
            Object obj = clazz.newInstance();
            Field[] declaredFields = obj.getClass().getDeclaredFields();
            Field[] superFields = obj.getClass().getSuperclass().getDeclaredFields();
            Field[] list = (Field[]) ArrayUtils.addAll(declaredFields,superFields);
            for (Field field : list) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }
                field.setAccessible(true);
                Class type = field.getType();
                if (null != map.get(field.getName())) {
                    if (type.getName().equals("java.lang.Long")) {
                        field.set(obj, Long.parseLong((String) map.get(field.getName())));
                    }
                    if (type.getName().equals("java.lang.Integer")) {
                        field.set(obj, Integer.parseInt((String) map.get(field.getName())));
                    }
                    if (type.getName().equals("java.lang.Double")) {
                        field.set(obj, Double.parseDouble((String) map.get(field.getName())));
                    }
                    if (type.getName().equals("java.math.BigDecimal")) {
                        field.set(obj, new BigDecimal((String) map.get(field.getName())));
                    }
                    if (type.getName().equals("java.lang.String")) {
                        field.set(obj, map.get(field.getName()));
                    }
                }
            }
            return obj;
        }


    public static void main(String[] args){

        WeChatBaseVo vo = new WeChatBaseVo();
        vo.setUsersLat("12345");
        vo.setUsersLng("132.5");
        try {
          Map<String,String>  result = ClazzChangeMap(vo);
            Iterator it = result.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                System.out.println("key=" + key + " value=" + value);
            }

          AppV3BaseVo appV3BaseVo = (AppV3BaseVo) MapChangeClazz(AppV3BaseVo.class,result);
            System.out.println(appV3BaseVo.getUsersLat());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
